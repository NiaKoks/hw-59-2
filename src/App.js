import React, { Component } from 'react';
import './App.css';
import Btn from './components/Btn'
import Jokes from './components/Jokes'

class App extends Component {

  state = {
    jokes:[],
  }

  jokesLoader =()=>{
    fetch('https://api.chucknorris.io/jokes/random').then(response =>{
      if(response.ok){
        return response.json()
      }
      throw new Error ('Something wrong with request')
    }).then(jokes =>{
      this.setState({jokes:jokes.value});
    }).catch(error =>{
      console.log(error);
    })
  }

  componentDidMount(){
    this.jokesLoader();
  }

  render() {
    return (
        <div className="App">
          <Jokes jokes ={this.state.jokes}/>
          <Btn newjoke={() => this.jokesLoader()}/>
        </div>
    );
  }
}
export default App