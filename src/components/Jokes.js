import React, {Component} from 'react';

class Jokes extends Component {
    render() {
        return (
            <div className="Jokes">
                <h3 className="Title">True Chuck Jokes:</h3>
                <p>{this.props.jokes}</p>
            </div>
        );
    }
}

export default Jokes;
